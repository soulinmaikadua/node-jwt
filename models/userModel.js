const mongoose = require("mongoose");

const User = mongoose.model(
  "user",
  new mongoose.Schema({
    firstname: String,
    lastname: String,
    email: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: String,
    },
    role: {
      type: String,
      default: "ADMIN",
    },
  })
);

module.exports = User;
