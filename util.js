const jwt = require("jsonwebtoken");

const verifyToken = async (req, res, next) => {
  let token = req.headers["authorization"];

  if (!token) {
    return res.status(403).json({ message: "No token provided!" });
  }
  //   console.log("token", token);
  const tokenSplit = token.split(" ");
  //   console.log("tokenSlice:", tokenSplit);
  const valid = jwt.verify(tokenSplit[1], "APP_SECRET");
  if (!valid) {
    return res.status(401).json({ message: "Not Authorized..." });
  }
  req.userId = valid.id;
  next();
};

const token = async (req) => {
  let token = req.headers["authorization"];

  if (!token) {
    return res.status(403).json({ message: "No token provided !" });
  }
  const tokenSplit = token.split(" ");
  console.log("tokenSlice:", tokenSplit);
  const valid = await jwt.verify(tokenSplit[1], "APP_SECRET");
  if (!valid) {
    return res.status(401).json({ message: "Not Authorized..." });
  }
  return valid.id;
};

const returnRole = async (req) => {
  let token = req.headers["authorization"];

  if (!token) {
    return res.status(403).json({ message: "No token provided !" });
  }
  const tokenSplit = token.split(" ");
  console.log("tokenSlice:", tokenSplit);
  const data = {
    id: 123,
    data: {
      name: "sou",
    },
    role: "ADMIN",
  };

  data.id;
  data.data.name;
  const {
    id,
    data: { name },
  } = data;

  const { role } = await jwt.verify(tokenSplit[1], "APP_SECRET");

  if (!role) {
    return res.status(401).json({ message: "Not Authorized..." });
  }
  return role;
};

module.exports = {
  verifyToken,
  token,
  returnRole,
};
