const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require("cors");
const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const User = require("./models/userModel");
const url = "mongodb://localhost:27017/node_jwt";
mongoose
  .connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: true,
    useCreateIndex: true,
  })
  .then((res) => console.log("MongoDB connected..."))
  .catch((e) => console.log(e));
app.get("/", (req, res) => {
  res.send("Hello world");
});

// require
const user = require("./routes/user.routes");
const auth = require("./routes/auth.routes");
app.use("/users", user);
app.use("/auth", auth);

const port = process.env.PORT || 2020;
app.listen(port, () => console.log(`Server running on port: ${port}`));
