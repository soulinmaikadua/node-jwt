const express = require("express");
const User = require("../models/userModel");
const router = express.Router();

const { verifyToken, token } = require("../util");

router.get("/", verifyToken, async (req, res) => {
  try {
    req.userId;
    const user = await User.find({});
    res.status(200).json(user);
  } catch (error) {
    console.log("querySingleUser.error : ", error);
  }
});
router.get("/:id", async (req, res) => {
  try {
    const userId = await token(req);
    console.log("console", userId);
    const { id } = req.params;
    const user = await User.findById(id);
    res.status(200).json(user);
  } catch (error) {
    console.log("querySingleUser.error : ", error);
  }
});
module.exports = router;
