const express = require("express");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const User = require("../models/userModel");
const router = express.Router();

router.post("/register", async (req, res) => {
  try {
    const { firstname, lastname, email, password } = req.body;
    const hash = await bcrypt.hash(password, 10);
    const newData = {
      firstname,
      lastname,
      email,
      password: hash,
    };
    const user = await User(newData);
    await user.save();
    const token = await jwt.sign(
      { id: user._id, role: user.role },
      "APP_SECRET"
    );
    const response = {
      user,
      token,
    };
    res.status(200).json(response);
  } catch (error) {
    console.log(`createUser.error : ${error}`);
  }
});

router.post("/login", async (req, res) => {
  try {
    const { email, password } = req.body;
    const user = await User.findOne({ email });
    if (!user) {
      res.status(500).json({ message: "No user found !" });
    }
    const validPwd = await bcrypt.compare(password, user.password);
    if (!validPwd) {
      res.status(500).json({ message: "Invalid password !" });
    }
    const token = await jwt.sign({ id: user._id }, "APP_SECRET", {
      expiresIn: 100000,
    });
    const response = {
      user,
      token,
    };
    res.status(200).json(response);
  } catch (error) {
    console.log(`createUser.error : ${error}`);
  }
});

module.exports = router;
